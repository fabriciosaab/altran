<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['nome','descricao','preco'];

    public function pedido() {
		return $this->hasMany('App\Pedido');
	}
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});


//Rotas para o Produto
Route::group(['prefix'=>'produto'], function() {
	Route::get('','ProdutoController@index');
	Route::get('create','ProdutoController@create');
	Route::post('store','ProdutoController@store');
	Route::get('{id}/destroy','ProdutoController@destroy');
	Route::get('{id}/edit','ProdutoController@edit');
	Route::put('{id}/update','ProdutoController@update');
});

//Rotas para o Cliente
Route::group(['prefix'=>'cliente'], function() {
	Route::get('','ClienteController@index');
	Route::get('create','ClienteController@create');
	Route::post('store','ClienteController@store');
	Route::get('{id}/destroy','ClienteController@destroy');
	Route::get('{id}/edit','ClienteController@edit');
	Route::put('{id}/update','ClienteController@update');
});

//Rotas para o Pedido
Route::group(['prefix'=>'pedido'], function() {
	Route::get('','PedidoController@index');
	Route::get('create','PedidoController@create');
	Route::post('store','PedidoController@store');
	Route::get('{id}/destroy','PedidoController@destroy');
	Route::get('{id}/edit','PedidoController@edit');
	Route::put('{id}/update','PedidoController@update');
});
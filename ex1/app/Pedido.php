<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = ['produto_id','cliente_id'];

    public function produto()
	{
		return $this->belongsTo('App\Produto');
	}

	public function cliente()
	{
		return $this->belongsTo('App\Cliente');
	}
}

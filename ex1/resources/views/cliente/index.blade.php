@extends('layouts.basico')

@section('content')
	<h1>Lista de Clientes</h1>

	<div class="panel panel-default">
  	<!-- Default panel contents -->
  	<div class="panel-heading"></div>

		<table class="table"> 
			<thead> 
				<tr> 

					<th>Nome</th> 
					<th>e-mail</th> 
					<th>Telefone</th> 

				</tr> 
			</thead> 
			<tbody>
				@foreach($clientes as $cliente) 
				<tr>

					<td>{{$cliente->nome}}</td>
					<td>{{$cliente->email}}</td>
					<td>{{$cliente->telefone}}</td>				
				
				</tr> 
				@endforeach
			</tbody> 
		</table>
	</div>

	<a href="{!!URL::to('/cliente/create')!!}"><button type="button" class="btn btn-primary">Adicionar Cliente</button></a>
	
@stop
@extends('layouts.basico')

@section('content')
	<h1>Lista de Pedidos</h1>

	<div class="panel panel-default">
  	<!-- Default panel contents -->
  	<div class="panel-heading"></div>

		<table class="table"> 
			<thead> 
				<tr> 

					<th>N. Pedido</th> 
					<th>Cliente</th> 
					<th>Produto</th> 

				</tr> 
			</thead> 
			<tbody>
				@foreach($pedidos as $pedido) 
				<tr>

					<td>{{$pedido->id}}</td>
					<td>{{$pedido->cliente->nome}}</td>
					<td>{{$pedido->produto->nome}}</td>
				
				</tr> 
				@endforeach
			</tbody> 
		</table>
	</div>
	
@stop
@extends('layouts.basico')

@section('content')
	<h1>Cadastro de Cliente</h1>

		{!! Form::open(['url'=>'cliente/store']) !!}
		<!-- Nome Form Input -->
		<div class="form-group">
		{!! Form::label('nome', 'Nome:') !!}
		{!! Form::text('nome', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('email', 'e-mail:') !!}
		{!! Form::text('email', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('telefone', 'Telefone:') !!}
		{!! Form::text('telefone', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::submit('Adicionar Cliente', ['class'=>'btn btn-primary']) !!}
		</div>
		{!! Form::close() !!}

@stop
<?php $ctr = substr(class_basename(Route::currentRouteAction()), 0, (strpos(class_basename(Route::currentRouteAction()), '@') -0) ); ?>
<html>
    <head>
        <title>App Name - @yield('title')</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        
    </head>
    <body>

        <div class="bs-example bs-navbar-top-example" data-example-id="navbar-fixed-to-top"> 
            <nav class="navbar navbar-default navbar-fixed-top">  
                <div class="container-fluid"> 
                    <div class="navbar-header"> 
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
                        <a class="navbar-brand" href="#">Teste Altran</a> 
                    </div> 
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6"> 
                        <ul class="nav navbar-nav">
                            @if ($ctr == "ClienteController")
                                <li class="active"><a href="{!!URL::to('/cliente')!!}">Cliente</a></li> 
                            @else
                                <li><a href="{!!URL::to('/cliente')!!}">Cliente</a></li> 
                            @endif

                            @if ($ctr == "ProdutoController")
                                <li class="active"><a href="{!!URL::to('/produto')!!}">Produto</a></li>
                            @else
                                <li><a href="{!!URL::to('/produto')!!}">Produto</a></li>
                            @endif

                            @if ($ctr == "PedidoController")
                                <li class="active"><a href="{!!URL::to('/pedido')!!}">Pedido</a></li> 
                            @else
                                <li><a href="{!!URL::to('/pedido')!!}">Pedido</a></li> 
                            @endif                            
                        </ul> 
                    </div> 
                </div> 
            </nav> 
        </div>

        <div class="container" style="padding-top:70px">
            @yield('content')
        </div>

    </body>
</html>
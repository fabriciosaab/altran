@extends('layouts.basico')

@section('content')
	<h1>Cadastro de Produtos</h1>

		{!! Form::open(['url'=>'produto/store']) !!}
		<!-- Nome Form Input -->
		<div class="form-group">
		{!! Form::label('nome', 'Nome:') !!}
		{!! Form::text('nome', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('descricao', 'Descrição:') !!}
		{!! Form::textarea('descricao', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('preco', 'Preço:') !!}
		{!! Form::text('preco', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
		{!! Form::submit('Criar Produto', ['class'=>'btn btn-primary']) !!}
		</div>
		{!! Form::close() !!}

@stop
@extends('layouts.basico')

@section('content')
	<h1>Lista de Produtos</h1>

	<div class="panel panel-default">
  	<!-- Default panel contents -->
  	<div class="panel-heading"></div>

		<table class="table"> 
			<thead> 
				<tr> 

					<th>Nome</th> 
					<th>Descrição</th> 
					<th>Preço</th> 

				</tr> 
			</thead> 
			<tbody>
				@foreach($produtos as $produto) 
				<tr>

					<td>{{$produto->nome}}</td>
					<td>{{$produto->descricao}}</td>
					<td>R$ {{$produto->preco}}</td>				
				
				</tr> 
				@endforeach
			</tbody> 
		</table>
		
	</div>
	<a href="{!!URL::to('/produto/create')!!}"><button type="button" class="btn btn-primary">Criar Produto</button></a>
@stop
Teste de PHP Altran Consultoria Fabricio Saab

# EX 1 - Sistema Gerenciador de Estoque

## Instalação

+ Clonar o Repositório

        $ git clone https://fabriciosaab@bitbucket.org/fabriciosaab/altran.git

+ Executar o seguinte comando

        $ composer install
    
+ Modificar o nome do arquivo __.env.example.__ por __.env__ e colocar as credênciais.

+ Executar as migrações.

        $ php artisan migrate

+ Iniciar o servidor

        $ php artisan serve

# EX 2 - Front-end 4 Rodas #
        $ cd ex2/